'use strict';

const RpcClientBase = require('bitcoind-rpc');
const { promisify } = require('util');

/**
 * Creates an RpcClient instance.
 *
 * @param {Object} options Configuration options
 * @param {string} options.protocol The protocol to use
 * @param {string} options.user The RPC server user name
 * @param {string} options.pass The RPC server user password
 * @param {string} options.host The domain name or IP address of the RPC server
 * @param {number} options.port The port of the RPC server
 * @constructor
 * @public
 */
function RpcClient(options) {
  if (!(this instanceof RpcClient)) return new RpcClient(options);

  RpcClientBase.call(this, options);
}

Object.setPrototypeOf(RpcClient.prototype, RpcClientBase.prototype);
Object.setPrototypeOf(RpcClient, RpcClientBase);

for (const key of Object.keys(RpcClientBase.callspec)) {
  const fn = promisify(RpcClientBase.prototype[key]);

  RpcClient.prototype[key] = async function() {
    const { result } = await Reflect.apply(fn, this, arguments);
    return result;
  };
  RpcClient.prototype[key.toLowerCase()] = RpcClient.prototype[key];
}

module.exports = RpcClient;
